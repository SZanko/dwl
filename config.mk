# Default compile flags (overridable by environment)
CFLAGS ?= -g -Wall -Wextra -Werror -Wno-unused-parameter -Wno-sign-compare -Wno-error=unused-function

PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

DESKTOP?= /usr/share/wayland-sessions
# Uncomment to build XWayland support
#CFLAGS += -DXWAYLAND
